package example.com.workwithpdf;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by SetKrul on 17.12.2015.
 */
public class CreatePdf {
    Context mContext;
    Document document;
    Bitmap sign;
    Font ffRedBold12;
    Font ffRedNormal10;

    Font ffBlueNormal12;

    Font ffBlackNormal8;
    Font ffBlackNormal9;
    Font ffBlackNormal10;

    Font ffWhiteNormal9;
    Font ffWhiteBold10;

    String move_for_less;
    String please_read_carefully_invoice;
    String please_read_carefully;
    String company_requisites;
    String job_number;
    String quote;
    String phone;
    String fax;
    String email;
    String attention;
    String estimation_total;
    String estimated_actual;

    String post_title;
    String agent_name;
    String agent_phone;
    String agent_fax;
    String agent_email;
    String startDate;
    String startTime;
    String stopDate;
    String stopTime;

    String fromFullName;
    String toFullName;
    String fromAddress;
    String toAddress;
    String fromApt;
    String toApt;
    String fromCity;
    String toCity;
    String fromState;
    String toState;
    String fromZip;
    String toZip;
    String fromPhone;
    String toPhone;

    String packingMaterials;
    String numberMen;
    String numberTruck;
    String localTravel;
    String localLabor;
    String sumTime;
    String localRate;
    String fuel;
    String estimatedTotal;
    String totalSum;
    String notes;

    public CreatePdf(Context _context, Bitmap _sign) {
        this.mContext = _context;
        this.sign = _sign;
    }

    public void jobData() {
        post_title = "L04531";
        agent_name = "Soslan";
        agent_phone = "111-111-111";
        agent_fax = "222-222-222";
        agent_email = "test@test.com";

        startDate = "17/12/2015";
        startTime = "08:00 AM";
        stopDate = "17/12/2015";
        stopTime = "08:00 PM";

        fromFullName = "User1";
        toFullName = "User2";

        fromAddress = "Street, 5";
        toAddress = "Street, 5";
        fromApt = "123";
        toApt = "321";
        fromCity = "Kiev";
        toCity = "Kiev";
        fromState = "UA";
        toState = "UA";
        fromZip = "60300";
        toZip = "60300";
        fromPhone = "111-111-111";
        toPhone = "111-111-111";

        packingMaterials = "5.25";
        numberMen = "2";
        numberTruck = "1";
        localTravel = "0.00";
        localLabor = "0.00";
        sumTime = "5";
        localRate = "65.00";
        fuel = "45.70";
        estimatedTotal = "325.00";
        totalSum = "370.70";
        notes = "Hi!";
    }

    public void setFontFactory() {
        ffRedBold12 = FontFactory.getFont(FontFactory.SYMBOL, 12, Font.BOLD, BaseColor.RED);
        ffBlackNormal8 = FontFactory.getFont(FontFactory.TIMES, 8, Font.NORMAL, BaseColor.BLACK);
        ffBlueNormal12 = FontFactory.getFont(FontFactory.TIMES, 12, Font.NORMAL, BaseColor.BLUE);
        ffBlackNormal10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL, BaseColor.BLACK);
        ffRedNormal10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL, BaseColor.RED);
        ffBlackNormal9 = FontFactory.getFont(FontFactory.TIMES, 9, Font.NORMAL, BaseColor.BLACK);
        ffWhiteBold10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.BOLD, BaseColor.WHITE);
        ffWhiteNormal9 = FontFactory.getFont(FontFactory.TIMES, 9, Font.NORMAL, BaseColor.WHITE);
    }

    public void findStringResources() {
        move_for_less = mContext.getResources().getString(R.string.move_for_less);
        please_read_carefully_invoice = mContext.getResources().getString(R.string.please_read_carefully_invoice);
        please_read_carefully = mContext.getResources().getString(R.string.please_read_carefully);
        company_requisites = mContext.getResources().getString(R.string.company_requisites);
        job_number = mContext.getResources().getString(R.string.job_number);
        quote = mContext.getResources().getString(R.string.quote_invoice);
        phone = mContext.getResources().getString(R.string.phone_invoice);
        fax = mContext.getResources().getString(R.string.fax_invoice);
        email = mContext.getResources().getString(R.string.email_invoice);
        attention = mContext.getResources().getString(R.string.attention);
        estimation_total = mContext.getString(R.string.estimated_total);
        estimated_actual = mContext.getString(R.string.actual_totals);
    }

    public File createPdf() throws DocumentException, IOException {
        findStringResources();
        jobData();
        setFontFactory();
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath());
        File file = new File(dir, "sample2.pdf");
        document = new Document(PageSize.A4, 35, 35, 35, 35);

        PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        document.add(addHeader());
        document.add(addSpace(15f));
        document.add(addAttention());
        document.add(addSpace(15f));
        document.add(addFromTo());
        document.add(addSpace(15f));
        document.add(addDetails());
        document.add(addSpace(15f));
        document.add(addNotes());
        document.add(addSpace(30f));
        document.add(addTextLine(please_read_carefully_invoice, ffRedBold12, Element.ALIGN_LEFT));
        document.add(addTextLine((please_read_carefully), ffBlackNormal8, Element.ALIGN_JUSTIFIED));
        document.add(addSpace(50f));
        document.add(addSign());
        document.close();
        return file;
    }

    public PdfPTable addHeader() {
        PdfPTable table = new PdfPTable(new float[]{1, 1.5f, 1});
        table.setWidthPercentage(100);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell cell;

        cell = new PdfPCell();
        cell.setBorderWidth(0f);
        cell.setImage(getImage(mContext, R.drawable.logo, 237f, 48f));
        cell.setPadding(10f);
        table.addCell(cell);

//        PdfPCell cell;
//        cell = setPadding(setBoarder(newCellWithImage(mContext, R.drawable.logo, 237f, 48f), 0f), 10f);
//        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setPadding(setBoarder(
                        newCell(move_for_less, ffBlueNormal12, Element.ALIGN_CENTER), 0f), 0f),
                setPadding(setBoarder(
                        newCell(company_requisites, ffBlackNormal10, Element.ALIGN_CENTER), 0f), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_LEFT, new PdfPCell[]{
                setPadding(setBoarder(
                        newCell(" " + job_number + " " + post_title, ffRedNormal10, Element.ALIGN_LEFT), 0f), 0f),
                setBoarder(
                        newCell(quote + agent_name + phone + " " +  agent_phone + fax + " " + agent_fax + email + " " + agent_email,
                        ffBlackNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(setBoarder(cell, 0f));
        return table;
    }

    public PdfPTable addAttention() {
        PdfPTable table = new PdfPTable(new float[]{1});
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = setBoarder(
                newCell(attention, ffBlackNormal9, Element.ALIGN_JUSTIFIED), 0f, 0f, 1f, 1f);
        table.addCell(cell);
        return table;
    }

    public PdfPTable addFromTo() {
        PdfPTable table = new PdfPTable(new float[]{1, 1});
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{
                newCell("From:" + " ", ffWhiteBold10, Element.ALIGN_LEFT, "#0086DA"),
                setBoarder(
                        newCell("Date Pickup Serv. " + "" + "Req'd: ", ffBlackNormal10, Element.ALIGN_LEFT), 0f),
                setBoarder(
                        newCell(startDate + " @ " + startTime, ffRedNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{
                newCell("To: " + "", ffWhiteBold10, Element.ALIGN_LEFT, "#0086DA"),
                setBoarder(
                        newCell("Date Pickup Serv. Req'd: ", ffBlackNormal10, Element.ALIGN_LEFT), 0f),
                setBoarder(
                        newCell(stopDate + " @ " + stopTime, ffRedNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(cell);

        cell = setBoarder(newCell(fromFullName, ffBlackNormal10, Element.ALIGN_LEFT), 0f, 0.5f, 0f, 0.5f);
        table.addCell(cell);

        cell = setBoarder(newCell(toFullName, ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0f, 0.5f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{
                setBoarder(
                        newCell(fromAddress, ffBlackNormal10, Element.ALIGN_LEFT), 0f),
                setBoarder(
                        newCell("Apt. No.: " + fromApt, ffBlackNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(setBoarder(cell, 0f, 0.5f, 0f, 0.5f));

        cell = new PdfPCell(addTableToCell(new float[]{1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{
                setBoarder(
                        newCell(toAddress, ffBlackNormal10, Element.ALIGN_LEFT), 0f),
                setBoarder(
                        newCell("Apt. No.: " + toApt, ffBlackNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(setBoarder(cell, 0.5f, 0f, 0f, 0.5f));

        cell = setBoarder(
                newCell(fromCity + ", " + fromState + ", " + fromZip, ffBlackNormal10, Element.ALIGN_LEFT),
                0f, 0.5f, 0f, 0.5f);
        table.addCell(cell);

        cell = setBoarder(
                newCell(toCity + ", " + toState + ", " + toZip, ffBlackNormal10, Element.ALIGN_LEFT),
                0.5f, 0f, 0f, 0.5f);
        table.addCell(cell);

        cell = setBoarder(
                newCell(fromPhone, ffBlackNormal10, Element.ALIGN_LEFT), 0f, 0.5f, 0f, 0.5f);
        table.addCell(cell);

        cell = setBoarder(
                newCell(toPhone, ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0f, 0.5f);
        table.addCell(cell);
        return table;
    }

    public PdfPTable addDetails() {
        PdfPTable table = new PdfPTable(new float[]{1, 1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setWidthPercentage(100);

        table.addCell(newCell(estimation_total, ffWhiteNormal9, Element.ALIGN_CENTER, "#0086DA"));

        table.addCell(newCell(mContext.getString(R.string.actual_totals), ffWhiteNormal9, Element.ALIGN_CENTER, "#0086DA"));

        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{
                newCell("ADDITIONAL SERVICES", ffBlackNormal10, Element.ALIGN_RIGHT),
                newCell(packingMaterials, ffBlackNormal10, Element.ALIGN_RIGHT)}));

        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{
                newCell("ADDITIONAL SERVICES", ffBlackNormal10, Element.ALIGN_RIGHT),
                newCell("0" + ".00", ffBlackNormal10, Element.ALIGN_RIGHT)}));

        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("LABOR CHARGE for" +
                " " + numberMen + " Men " + numberTruck + " Truck @ " + sumTime +
                " hours x " + localRate + " ea", ffBlackNormal10, Element.ALIGN_RIGHT, "#a9a8a8"), newCell
                (estimatedTotal,
                        ffBlackNormal10, Element.ALIGN_RIGHT, "#a9a8a8")}));
        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("LABOR",
                ffBlackNormal10, Element.ALIGN_RIGHT, "#a9a8a8"), newCell("0" + ".00", ffBlackNormal10, Element
                .ALIGN_RIGHT, "#a9a8a8")}));

        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("FUEL CHARGE",
                ffBlackNormal10, Element.ALIGN_RIGHT, "#a9a8a8"), newCell(fuel, ffBlackNormal10, Element
                .ALIGN_RIGHT, "#a9a8a8")}));
        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("FUEL CHARGE",
                ffBlackNormal10, Element.ALIGN_RIGHT, "#a9a8a8"), newCell("0" + ".00", ffBlackNormal10, Element
                .ALIGN_RIGHT, "#a9a8a8")}));

        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("QUOTE TOTAL",
                ffBlackNormal10, Element.ALIGN_RIGHT), newCell(totalSum, ffBlackNormal10, Element.ALIGN_RIGHT,
                "#D6BF14")}));
        table.addCell(addTableToCell(new float[]{2, 1}, Element.ALIGN_RIGHT, new PdfPCell[]{newCell("QUOTE TOTAL",
                ffBlackNormal10, Element.ALIGN_RIGHT), newCell("0" + "" +
                ".00", ffBlackNormal10, Element.ALIGN_RIGHT)}));
        return table;
    }


    public PdfPTable addNotes() {
        PdfPTable table = new PdfPTable(new float[]{1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setWidthPercentage(100);
        table.addCell(newCell("CUSTOMER NOTES", ffWhiteNormal9, Element.ALIGN_CENTER, "#0086DA"));
        table.addCell(newCell(notes, ffBlackNormal10, Element.ALIGN_LEFT));
        return table;
    }

    public PdfPTable addSign() {
        PdfPTable table = new PdfPTable(new float[]{1, 1, 1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.setWidthPercentage(100);

        PdfPCell cell;

        cell = new PdfPCell(addTableToCell(new float[]{1, 0.3f}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell("Customer Service", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f, 0f, 0f, 0.5f), setBoarder
                (newCell(null, null, 0), 0f)}));
        cell.setPaddingTop(30f);
        table.addCell(setBoarder(cell, 0f));

//        cell = new PdfPCell();
//        cell = setBoarder(cell, 0f, 0f, 0f, 0.5f);
//        cell.setImage(getImage(mContext, R.drawable.logo, 237f, 48f));
//        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder(newCell
                (null, null, 0), 0f), setBoarder(newCellWithImage(mContext, sign, 237f, 48f), 0f, 0f, 0f, 0.5f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell(null, null, 0), 0f), setBoarder(newCell(stopDate, ffBlackNormal10, Element.ALIGN_BOTTOM)
                , 0f, 0f, 0f, 0.5f)}));
        cell.setPaddingTop(30f);
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1, 0.3f}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell("Customer Name", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f), setBoarder(newCell(null, null,
                0), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = setBoarder(newCell(null, null, 0), 0f);
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell(null, null, 0), 0f), setBoarder(newCell("Date", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        return table;
    }


    public PdfPCell setBoarder(PdfPCell _cell, float _left, float _right, float _top, float _bottom) {
        _cell.setBorderWidthLeft(_left);
        _cell.setBorderWidthBottom(_bottom);
        _cell.setBorderWidthRight(_right);
        _cell.setBorderWidthTop(_top);
        return _cell;
    }

    public PdfPCell setBoarder(PdfPCell _cell, float _boarder) {
        _cell.setBorderWidth(_boarder);
        return _cell;
    }

    public PdfPCell setPadding(PdfPCell _cell, float _left, float _right, float _top, float _buttom) {
        _cell.setPaddingLeft(_left);
        _cell.setPaddingBottom(_buttom);
        _cell.setPaddingRight(_right);
        _cell.setPaddingTop(_top);
        return _cell;
    }

    public PdfPCell setPadding(PdfPCell _cell, float _padding) {
        _cell.setPadding(_padding);
        return _cell;
    }

    public PdfPCell addTableToCell(float[] _floats, int _align, PdfPCell[] _pdfPCell) {
        PdfPTable table = new PdfPTable(_floats);
        table.getDefaultCell().setHorizontalAlignment(_align);
        table.setWidthPercentage(100);
        for (PdfPCell a_pdfPCell : _pdfPCell) {
            table.addCell(a_pdfPCell);
        }
        PdfPCell cell;
        cell = new PdfPCell(table);
        cell.setPadding(0f);
        return cell;
    }

    public PdfPCell newCell(String _s, Font _font, int _align, String _color) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(_s, _font));
        cell.setHorizontalAlignment(_align);
        if (!TextUtils.isEmpty(_color)) {
            cell.setBackgroundColor(WebColors.getRGBColor(_color));
        }
        return cell;
    }

    public String getColor(int _color){
        return String.format("#%06X", 0xFFFFFF & mContext.getResources().getColor(_color));
    }

    public PdfPCell newCell(String _s, Font _font, int _alignCenter) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(_s, _font));
        cell.setHorizontalAlignment(_alignCenter);
        return cell;
    }

    public PdfPCell newCellWithImage(Context _context, int _logo, float _height, float _width) {
        PdfPCell cell;
        cell = new PdfPCell();
        cell.setImage(getImage(_context, _logo, _height, _width));
        return cell;
    }

    public PdfPCell newCellWithImage(Context _context, Bitmap _logo, float _height, float _width) {
        PdfPCell cell;
        cell = new PdfPCell();
        cell.setImage(getImage(_context, _logo, _height, _width));
        return cell;
    }

    public PdfPCell addToCell(PdfPCell _cell, String _s, Font _font) {
        Phrase firstLine = new Phrase(_s, _font );
        _cell.addElement(firstLine);
        return _cell;
    }

    public Paragraph addSpace(float _space) {
        Paragraph paragraph = new Paragraph(_space);
        paragraph.setSpacingBefore(_space);
        return paragraph;
    }

    public Paragraph addTextLine(String s, Font _font, int _alignLeft) {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(_alignLeft);
        paragraph.add(new Anchor(s, _font));
        return paragraph;
    }

    public Image getImage(Context _context, int _image, float _height, float _width) {
        Drawable d = ContextCompat.getDrawable(_context, _image);
        BitmapDrawable bitDw = ((BitmapDrawable) d);
        Bitmap bmp = bitDw.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = null;
        try {
            image = Image.getInstance(stream.toByteArray());
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        if (image != null) {
            image.scaleAbsolute(_height, _width);
        }
        return image;
    }

    public Image getImage(Context _context, Bitmap _image, float _height, float _width) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Image image = null;
        if (_image != null) {
            _image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            try {
                image = Image.getInstance(stream.toByteArray());
            } catch (BadElementException | IOException e) {
                e.printStackTrace();
            }
            if (image != null) {
                image.scaleAbsolute(_height, _width);
            }
        }
        return image;
    }
}


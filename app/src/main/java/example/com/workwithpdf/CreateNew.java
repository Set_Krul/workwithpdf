package example.com.workwithpdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by SetKrul on 17.12.2015.
 */
public class CreateNew {
    Context mContext;
    Document document;
    Bitmap sign;
    Font ffRedBold12;
    Font ffRedNormal10;

    Font ffBlueNormal12;

    Font ffBlackNormal5;
    Font ffBlackNormal7;
    Font ffBlackNormal8;
    Font ffBlackNormal9;
    Font ffBlackNormal10;
    Font ffBlackNormal12;
    Font ffBlackBold12;

    Font ffWhiteNormal7;
    Font ffWhiteNormal9;
    Font ffWhiteBold10;

    String move_for_less;
    String please_read_carefully_invoice;
    String please_read_carefully;
    String company_requisites;
    String job_number;
    String quote;
    String phone;
    String fax;
    String email;
    String attention;
    String estimation_total;
    String estimated_actual;

    String post_title;
    String agent_name;
    String agent_phone;
    String agent_fax;
    String agent_email;
    String startDate;
    String startTime;
    String stopDate;
    String stopTime;

    String fromFullName;
    String toFullName;
    String fromAddress;
    String toAddress;
    String fromApt;
    String toApt;
    String fromCity;
    String toCity;
    String fromState;
    String toState;
    String fromZip;
    String toZip;
    String fromPhone;
    String toPhone;

    String packingMaterials;
    String numberMen;
    String numberTruck;
    String localTravel;
    String localLabor;
    String sumTime;
    String localRate;
    String fuel;
    String estimatedTotal;
    String totalSum;
    String notes;

    public CreateNew(Context _context, Bitmap _sign) {
        this.mContext = _context;
        this.sign = _sign;
    }

    public void jobData() {
        post_title = "L04531";
        agent_name = "Soslan";
        agent_phone = "111-111-111";
        agent_fax = "222-222-222";
        agent_email = "test@test.com";

        startDate = "17/12/2015";
        startTime = "08:00 AM";
        stopDate = "17/12/2015";
        stopTime = "08:00 PM";

        fromFullName = "User1";
        toFullName = "User2";

        fromAddress = "Street, 5";
        toAddress = "Street, 5";
        fromApt = "123";
        toApt = "321";
        fromCity = "Kiev";
        toCity = "Kiev";
        fromState = "UA";
        toState = "UA";
        fromZip = "60300";
        toZip = "60300";
        fromPhone = "111-111-111";
        toPhone = "111-111-111";

        packingMaterials = "5.25";
        numberMen = "2";
        numberTruck = "1";
        localTravel = "0.00";
        localLabor = "0.00";
        sumTime = "5";
        localRate = "65.00";
        fuel = "45.70";
        estimatedTotal = "325.00";
        totalSum = "370.70";
        notes = "Hi!";
    }

    public void setFontFactory() {
        ffRedBold12 = FontFactory.getFont(FontFactory.SYMBOL, 12, Font.BOLD, BaseColor.RED);
        ffBlackNormal8 = FontFactory.getFont(FontFactory.TIMES, 8, Font.NORMAL, BaseColor.BLACK);
        ffBlackNormal12 = FontFactory.getFont(FontFactory.TIMES, 12, Font.NORMAL, BaseColor.BLACK);
        ffBlueNormal12 = FontFactory.getFont(FontFactory.TIMES, 12, Font.NORMAL, BaseColor.BLUE);
        ffBlackNormal10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL, BaseColor.BLACK);
        ffRedNormal10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL, BaseColor.RED);
        ffBlackNormal9 = FontFactory.getFont(FontFactory.TIMES, 9, Font.NORMAL, BaseColor.BLACK);
        ffWhiteBold10 = FontFactory.getFont(FontFactory.TIMES, 10, Font.BOLD, BaseColor.WHITE);
        ffWhiteNormal9 = FontFactory.getFont(FontFactory.TIMES, 9, Font.NORMAL, BaseColor.WHITE);
        ffWhiteNormal7 = FontFactory.getFont(FontFactory.TIMES, 7, Font.NORMAL, BaseColor.WHITE);
        ffBlackNormal5 = FontFactory.getFont(FontFactory.TIMES, 5, Font.NORMAL, BaseColor.BLACK);
        ffBlackNormal7 = FontFactory.getFont(FontFactory.TIMES, 7, Font.NORMAL, BaseColor.BLACK);
        ffBlackBold12 = FontFactory.getFont(FontFactory.TIMES, 7, Font.NORMAL, BaseColor.BLACK);
    }

    public void findStringResources() {
        move_for_less = mContext.getResources().getString(R.string.move_for_less);
        please_read_carefully_invoice = mContext.getResources().getString(R.string.please_read_carefully_invoice);
        please_read_carefully = mContext.getResources().getString(R.string.please_read_carefully);
        company_requisites = mContext.getResources().getString(R.string.company_requisites);
        job_number = mContext.getResources().getString(R.string.job_number);
        quote = mContext.getResources().getString(R.string.quote_invoice);
        phone = mContext.getResources().getString(R.string.phone_invoice);
        fax = mContext.getResources().getString(R.string.fax_invoice);
        email = mContext.getResources().getString(R.string.email_invoice);
        attention = mContext.getResources().getString(R.string.attention);
        estimation_total = mContext.getString(R.string.estimated_total);
        estimated_actual = mContext.getString(R.string.actual_totals);
    }

    public File createPdf() throws DocumentException, IOException {
        findStringResources();
        jobData();
        setFontFactory();
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath());
        File file = new File(dir, "sample2.pdf");
        document = new Document(PageSize.A4, 15, 35, 35, 15);

        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();
        document.add(addHeader());
        document.add(addSpace(5f));
        document.add(addAttention());
        document.add(addSpace(5f));
        document.add(addFromTo());
        document.add(addSpace(5f));
        document.add(addAttention2());
        document.add(addSpace(5f));
        document.add(addDetails());
        document.add(addSpace(5f));
        document.add(addCrap());
        document.add(addSpace(5f));
        document.add(addCrap2());
        document.add(addCrap3());
        pdfWriter.setPageEmpty(false);
        document.newPage();
        document.add(addCrap4());
//        document.add(addNotes());
//        document.add(addSpace(30f));
        document.add(addTextLine(mContext.getString(R.string.important_notice), ffBlackBold12, Element.ALIGN_CENTER));
        document.add(addTextLine(("The rates herein quoted are to be applied on the actual number o f hours, actual weight or actual number of other units of measurement (irrespective o f any estimate), and supersede any previous rate quotation, estimates or representations concerning charges to be assessed. The quotations have been made in good faith and arc correct to the best o f our knowledge. If. however, any rate herein quoted should be found to be in conflict, the carrier is current rate will supersede any rate quotation herein made" +
                "."), ffBlackNormal8, Element.ALIGN_JUSTIFIED));
        document.add(addTextLine("TERMS AND CONDITIONS", ffBlackBold12, Element.ALIGN_CENTER));
        document.add(addTextLine(("Sec. 1. OWNERSHIP OF PROPERTY:  The customer has represented and warranted to the " +
                "company that he is the legal owner or in lawful possession of the property and has the legal right " +
                "and authority to contract for services for all the property tendered, upon provisions, limitations, " +
                "terms and conditions herein set forth and that there are. no existing liens, mortgages of encumbrances on said property. If there be any litigation as a " +
                "result, of the breach o f this clause customer agrees to pay all charges that may be due together with such coos and expenses including attorney fees which this company may " +
                "reasonably incur or become liable to pay in connection therewith and this company shall have a lien on said property for all charges that, may be due them as well as for said" +
                " costs and expenses.\n" +
                "Sec. 2. PAYMENT:  Storage accounts are due and payable monthly in advance. Interest will be charged " +
                "on accounts unpaid for & period o f 3 months after they become due. The company has a lien on all goods moved or stored to secure payment for charges for all " +
                "services rendered. Ail charges must he paid in cash, money order or certified check before delivery " +
                "or transfer of goods deposited under this contract and no transfer of title " +
                "will be recognized unless entered on the books of this company.\n" +
                "Sec. 3. LIABILITY OF THE COMPANY;  (a) The company when transporting to or from the warehouse for " +
                "permanent storage acts as a private carrier only, reserving the right to refuse any order for " +
                "transportation and in no event is a common carrier.\n" +
                "\t(b) This contract is accepted subject to delay or damages caused by war, insurrection, labor " +
                "troubles, strikes. Acts of God or the public enemy, riots, the elements. street traffic, elevator " +
                "service or other causes beyond the control o f the company.\n" +
                "\t(c) The company is nor responsible for any fragile articles injured or broken, unless packed by " +
                "its " +
                "employees and unpacked by them at the time of delivery and in no event shall the company be liable except for its own negligence, The company will not be " +
                "responsible for mechanical or electrical functioning of any articles such as, but not limited to. pianos, radios, phonographs, television sets, clocks, barometers, mechanical " +
                "refrigerators or air conditioners whether or not such articles are packed or unpacked by the company.\n" +
                "\t(d) No liability of any kind shall attach to this company for any damage caused, to the goods by " +
                "inherent vice, moths, vermin or other insects, rust. fire, water, deterioration or normal wear and tear and mildew.\n" +
                "(e) Unless a greater valuation is stated herein, the depositor or owner declares that the value in " +
                "case o f loss or damage arising out o f storage, transportation, packing, unpacking, fumigation, cleaning or handling of the goods and the liability o f the company " +
                "for any claim for which it may be liable for each or any piece or package and the contents thereof does not exceed, and is limited to, that amount per lb. designated on the " +
                "front o f this contract, or, if no amount is designated, to 60 cents per lb. per article for the entire contents of the storage lot, upon which declared or agreed value the rates " +
                "are based, the depositor or owner having been given the opportunity to declare a higher valuation without limitation in case o f loss or damage front any cause which would make the " +
                "company liable and to pay the higher rate based thereon, and in no event shall the company he liable except for its own negligence.\n" +
                "\t(f) In the event the company shall be requested by the customer to engage the services o f others " +
                "with respect to the transportation, repair, cleaning or servicing of any article, the company shall act as agent for the customer and shall not be liable for any " +
                "damage arising out o f such services rendered by others and shall not be .liable for failure to execute any instructions except for such instructions that are in writing and " +
                "acknowledged in writing by the company. In the event the goods are delivered to another carrier, the" +
                " company shall not be liable.' for loss or damage for any cause to said goods unless exception is " +
                "noted in writing on the delivery receipt o f this company when delivery  made.\n" +
                "\t(g) The company shall not be responsible for loss o f or damage to any article contained in " +
                "drawers," +
                " or in packages, cases or containers not packed and unpacked by the employees o f the company unless such containers are opened for the company " +
                "inspection and then only for such articles that are specifically listed by the customer and are receipted for the company or its agent.\n" +
                "\t(h) fn no event shall the company be responsible for loss or damage to documents, stamps, " +
                "securities, specie or jewelry unless a special agreement in writing is made between the customer and the company with respect to such articles.\n" +
                "Sec, 4. MINIMUM PERIOD OF STORAGE:  On storage accounts, three months storage will, be charged for " +
                "any fraction o f the first three months period. Thereafter one months storage will be charged for each month or fraction thereof.\n" +
                "Sec. 5. TERMINATION OF  STORAGE:  The company reserves the right to terminate storage o f the goods " +
                "at any time by giving the depositor thirty days written notice of its intention to do so. and, unless the depositor removes such goods within that period, " +
                "the company is hereby empowered to have the same removed at the cost and expense of the depositor, and upon so doing, the company shall be relieved of any liability with " +
                "respect to such goods therefore and thereafter incurred.\n" +
                "Sec, 6, ADDRESS AND CHANGE:  It is agreed that the address of the depositor o f goods for storage is" +
                " as given on the front side of this contract and shall be relied upon by the company as the address of the depositor until change o f address is given in writing to " +
                "the company and acknowledged in writing by the company and notice of any change of address will not he valid or binding upon the company if given or acknowledged in any " +
                "other manner.\n" +
                "Sec, 7.  TIME FOR FILING  CLAIMS:  00 As a condition precedent to recovers, claims must be filed in " +
                "writing with the company within ten days after delivery of property and suit or arbitration, in accordance with the provisions of this contract, must: be " +
                "instituted within six months from the day when notice in writing is given by the company to the customer that the company has disallowed the claim or any part thereof.\n" +
                "\t(h) Where claims are not filed, nor suit or arbitration instituted in. accordance with the " +
                "provisions of this contract, the company shall not be liable for such claims and such claims will not be paid.\n" +
                "\t(c) The company shall have the right to inspect and. repair alleged damaged articles.\n" +
                "Sec.  8 .  GENERAL  TERMS AND  CONDITIONS;  (a) If goods cannot be delivered in the ordinary way by " +
                "stairs or elevators, the owner agrees to pay an additional charge for hoisting or lowering or other necessary labor to effect delivery. Customer shall arrange " +
                "in advance for all necessary elevator and other services and any charges for same shall be met by the customer. Customer agrees to pay hourly charge in this contract for waiting" +
                " time caused by lack of sufficient elevator service.\n" +
                "\t(h) Packing or moving charges do not include the taking down or putting up o f curtains, mirrors, " +
                "fixtures, pictures, electric or other fittings, or the relaying of \n" +
                "floor coverings or similar services but if such services are ordered u charge will be made therefore" +
                ".\n" +
                "\t(c)  Company will charge for labor and materials supplied on all access to goods in the warehouse" +
                ".\n" +
                "\t(d) Platform charge will be made when goods are delivered to an outside truckman.\n" +
                "Sec, 9. CORRECTION OF ERRORS;  The depositor agrees that unless notice is given in writing to the " +
                "company within 15 days after receipt o f the inventory list accompanying the warehouse receipt and made a part thereof including any exceptions noted thereon as" +
                " to the condition of the property when received for storage, the inventory list shall be deemed to " +
                "be correct and complete.\n" +
                "Sec.  10.  CONTROVERSY  O F  CLAIM:  Any  controversy or claim arising out o f o r relating to this " +
                "contract, the breach thereof, or the goods affected thereby, whether such claims be found in tort or contract shall be settled by arbitration law of the State of the " +
                "principle place of business of the company and under the rules of the American Arbitration Association, provided, however, that upon any such arbitration the arbitrator or " +
                "arbitrators may not vary or modify any of the foregoing provisions.\n" +
                "Sec. 11. AGREEMENT: The contract represents the entire agreement between the parties hereto and " +
                "cannot be modified except in writing and shall be deemed to apply to all the property whether household goods or goods of any other nature or description which the " +
                "company may now or any time in the future store, pack, transport, or ship for the owner's account.\n"), ffBlackNormal8, Element.ALIGN_JUSTIFIED));
        document.add(addTextLine("INSURANCE/FULL VALUE PROTECTION", ffBlackBold12, Element.ALIGN_LEFT));
        document.add(addTextLine(("STORAGE-LIM ITED  FULL  COVERAGE  -  This evidence  protects  against  all  risk  " +
                "of direct  physical  loss of or damage  to  the described property from any external cause  " +
                "including  incidental,  pick up  and  delivery  by  the  warehouseman's  vehicles  within  a  radius" +
                " o f fifty  miles  for household  goods  held      on deposit incidental   to transportation. No  " +
                "Insurance/Full  Value Protection  is placed unless requested and coverage appears on face hereof and" +
                " premium  is  paid. If protection so placed, subject  to existing conditions and exclusions " +
                "contained  in  the M aster  Policy  of Insurance  and/or Certificate of Full  Value  Protection, a " +
                "copy  of which  is  available  for inspection  at  the  office o f the company  accepting the  " +
                "property for storage. Coverage  applies to Household Goods  as defined  in  such  M aster Policy  " +
                "and/or Certificate, subject to  property excluded  therein and excluding accounts,  hills, currency," +
                " deeds, evidences o f debt, money, jewelry, notes or securities,  furs or garments  trimmed  with " +
                "fur. This  evidence or the  Master Policy  and/or Certificate, does not insure the described " +
                "property against: \n" +
                "\t(a)  Loss or damage from insects, moths, vermin, inherent vice, deterioration, dampness" +
                " of atmosphere, extremes of temperature, nor from ordinary  wear and  tear; or damage sustained " +
                "during any cleaning, repairing, restoration or retouching process, unless caused  by fire; or " +
                "spoilage of the contents of deep freezers, however caused: the  mechanical  or electrical  " +
                "derangement of television sets, radios, refrigerators, deep freezers, washing machines, unless " +
                "evidenced by external damage to such equipment. \n" +
                "\t(b)  Nor against, those  perils excluded  by  the  Nuclear Exclusion  Clause  and  War " +
                "Risk  Exclusion Clause  set  forth  in  the  printed  “Conditions'5 o f the Master  Policy  and/or " +
                "Certificate. \n" +
                "\tUnless caused by  accident or rough handling and there  is physical evidence o f " +
                "breakage o f brittle articles same  is not covered unless goods have been  packed and  unpacked, by " +
                "our Company . Goods must  be covered to full  value. In event  the actual cash  value of the  " +
                "property covered  being in excess o f the  amount declared, it  is agreed that the  shipper  shall  " +
                "be  regarded  as  his  or her own  Insurer  for  the difference and shall  bear that portion  o f " +
                "any  loss  which  the  noncovered  amount bears  to the  actual  cash  value of said property \n" +
                "\tInsurance  and/or Full  Value  Protection  may  be  cancelled  at  any  time  by  the  " +
                "customer  or  the  Company  in  compliance  with  those  sections o f the  Master  Policy   and/or " +
                "Certificate o f Full  Value Protection. Notice mailed to the last known address o f the customer " +
                "shall be deemed sufficient notice."), ffBlackNormal8, Element.ALIGN_JUSTIFIED));
//        document.add(addSpace(50f));
//        document.add(addSign());
        document.close();
        return file;
    }

    public PdfPTable addHeader() {
        PdfPTable table = new PdfPTable(new float[]{1.2f, 1f, 0.7f});
        table.setWidthPercentage(100);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell cell;

//        cell = new PdfPCell();
//        cell.setBorderWidth(0f);
//        cell.setImage(getImage(mContext, R.drawable.logo, 237f, 48f));
//        cell.setPadding(10f);
//        table.addCell(cell);

//        PdfPCell cell;
//        cell = setPadding(setBoarder(newCellWithImage(mContext, R.drawable.logo, 237f, 48f), 0f), 10f);
//        table.addCell(cell);

        cell = new PdfPCell(setPadding(setBoarder(newCell("WWW.MOVEFORLESS.BIZ\n\nContract for Service / Estimates " +
                "of" + " Moving Costs", ffBlackNormal10, Element.ALIGN_CENTER), 0f), 0f));
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{setPadding(setBoarder
                (newCell(move_for_less, ffBlackNormal12, Element.ALIGN_CENTER), 0f), 0f), setPadding(setBoarder
                (newCell("17854 NE 5th Ave. Miami, FL 33162\n" + "Tel:(305) 915-8881 Fax: (305) 514-0596",
                        ffBlackNormal10, Element.ALIGN_CENTER), 0f), 0f), setPadding(setBoarder(newCell("IM 1543     " +
                "    MR 564", ffBlackNormal10, Element.ALIGN_CENTER), 0f), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_RIGHT, new PdfPCell[]{setPadding(setBoarder
                (newCell("Job #____________", ffBlackNormal12, Element.ALIGN_LEFT), 0f), 0f), setBoarder
                (addTableToCell(new float[]{1, 0.2f}, Element.ALIGN_RIGHT, new PdfPCell[]{setPadding(setBoarder
                        (newCell("\nDate Serv. Req'd.", ffBlackNormal8, Element.ALIGN_LEFT), 0f, 0f, 0f, 1f), 2f),
                        setPadding(setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0f, 0f,
                                1f), 1f), setPadding(setBoarder(newCell("\nDate Serv. Req'd.", ffBlackNormal8,
                        Element.ALIGN_LEFT), 0f, 0f, 0f, 1f), 2f), setPadding(setBoarder(newCell("A.M.\nP.M.",
                        ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0f, 0f, 1f), 1f), setPadding(setBoarder(newCell
                        ("\nDate Serv. Req'd.", ffBlackNormal8, Element.ALIGN_LEFT), 0f, 0f, 0f, 1f), 2f), setPadding
                        (setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0f, 0f, 1f), 1f),
                        setPadding(setBoarder(newCell("\nDate Serv. Req'd.", ffBlackNormal8, Element.ALIGN_LEFT), 0f,
                                0f, 0f, 1f), 2f), setPadding(setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element
                        .ALIGN_RIGHT), 0f, 0f, 0f, 1f), 1f)}), 0f)}));
        table.addCell(setBoarder(cell, 0f));
        return table;
    }

    public PdfPTable addAttention() {
        PdfPTable table = new PdfPTable(new float[]{1});
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = setBoarder(newCell(attention, ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 1f, 1f);
        table.addCell(cell);
        return table;
    }

    public PdfPTable addFromTo() {
        PdfPTable table = new PdfPTable(new float[]{1, 1});
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = new PdfPCell(addTableToCell(new float[]{0.4f, 1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{newCell
                ("FROM:" + " ", ffWhiteBold10, Element.ALIGN_CENTER, "#000000"), setBoarder(newCell("NAME: ",
                ffBlackNormal10, Element.ALIGN_LEFT), 0f), setBoarder(newCell("", ffRedNormal10, Element.ALIGN_LEFT),
                0f)}));
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{0.4f, 1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{newCell("TO:",
                ffWhiteBold10, Element.ALIGN_CENTER, "#000000"), setBoarder(newCell("NAME: ", ffBlackNormal10,
                Element.ALIGN_LEFT), 0f), setBoarder(newCell("", ffRedNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(cell);

        cell = setBoarder(newCell("ADDRESS:", ffBlackNormal10, Element.ALIGN_LEFT), 0f, 0.5f, 0f, 0.5f);
        table.addCell(cell);

        cell = setBoarder(newCell("ADDRESS:", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0f, 0.5f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{setBoarder(newCell
                ("", ffBlackNormal10, Element.ALIGN_LEFT), 0f), setBoarder(newCell("TEL: " + fromApt,
                ffBlackNormal10, Element.ALIGN_LEFT), 0f)}));
        table.addCell(setBoarder(cell, 0f, 0.5f, 0f, 0.5f));

        cell = new PdfPCell(addTableToCell(new float[]{1, 1}, Element.ALIGN_LEFT, new PdfPCell[]{setBoarder(newCell
                ("", ffBlackNormal10, Element.ALIGN_LEFT), 0f), setBoarder(newCell("TEL: " + toApt, ffBlackNormal10,
                Element.ALIGN_LEFT), 0f)}));
        table.addCell(setBoarder(cell, 0.5f, 0f, 0f, 0.5f));
        return table;
    }


    public PdfPTable addAttention2() {
        PdfPTable table = new PdfPTable(new float[]{1});
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = setBoarder(newCell("PLEASE READ CAREFULLY: THIS CONTRACT FOR SERVICE IS REQUIRED BY COUNTY LAW " +
                "AND " +
                "MUST INCLUDE THE TERMS AND COSTS ASSOCIATED WITH YOUR MOVE. IN ORDER FR THE CONTRACT FOR SERVICE TO " +
                "BE ACCURATE, YOU MUST DISCLOSE ALL INFORMATION RELEVANT TO THE MOVE TO THE MOVER. COUNTY LAW " +
                "REQUIRES THAT A MOVER RELINQUISH POSSESSION OF YOUR GOODS AND COMPLETE YOUR MOVE UPON PAYMENT OF NO " +
                "MORE THAN THE SPECIFIED MAXIMUM AMOUNT DUE AT DELIVERY. UNDER COUNTY LAW YOU ARE ENTITLED TO A " +
                "WRITTEN ESTIMATE OF THE TOTAL COST OF YOUR MOVE AND A COPY OF THE DISCLOSURE STATEMENT. PLEASE " +
                "REVIEW THESE DOCUMENTS TO MAKE SURE THEY ARE COMPLETE. BY SIGNING THIS FROM, YOU ARE WAIVING CERTAIN" +
                " VALUABLE COVERAGE WHICH PROTECTS YOUR POSSESSIONS ABOVE THE MINIMUM AMOUNTS SET BY LAW.",
                ffBlackNormal8, Element.ALIGN_JUSTIFIED),
        0f, 0f,
                1f, 1f);
        table.addCell(cell);
        return table;
    }

    public PdfPTable addDetails() {
        PdfPTable table = new PdfPTable(new float[]{1, 1, 1, 1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setWidthPercentage(100);

        table.addCell(setBoarder(newCell("Description of Property:", ffWhiteNormal9, Element.ALIGN_LEFT, "#000000"),
                1f, BaseColor.WHITE));
        table.addCell(setBoarder(newCell("CONTAINERS", ffWhiteNormal9, Element.ALIGN_CENTER, "#000000"),
                1f, BaseColor.WHITE));
        table.addCell(setBoarder(newCell("PACKING", ffWhiteNormal9, Element.ALIGN_CENTER, "#000000"),
                1f, BaseColor.WHITE));
        table.addCell(setBoarder(newCell("UNPACKING", ffWhiteNormal9, Element.ALIGN_CENTER, "#000000"),
                1f, BaseColor.WHITE));


        table.addCell(setBoarder(newCell("ITEMS", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor
                .WHITE));
        table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setBoarder(newCell("QUANTITY", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("RATE", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("AMOUNT", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE)
        }));
        table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setBoarder(newCell("QUANTITY", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("RATE", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("AMOUNT", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE)
        }));
        table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setBoarder(newCell("QUANTITY", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("RATE", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE),
                setBoarder(newCell("AMOUNT", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 1f, BaseColor.WHITE)
        }));

        for (int i = 0; i < 15; i++) {
            table.addCell(setBoarder(newCell("qwertykl;", ffBlackNormal8, Element.ALIGN_LEFT), 0.5f));
            table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                    setBoarder(newCell("2", ffBlackNormal8, Element.ALIGN_RIGHT), 0f),
                    addTableToCell(new float[]{1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                            setBoarder(newCell("$", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                            setBoarder(newCell("4.00", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
                    }),
                    setBoarder(newCell("8.00", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
            }));
            table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                    setBoarder(newCell("3", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                    addTableToCell(new float[]{1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                            setBoarder(newCell("$", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                            setBoarder(newCell("4.00", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
                    }),
                    setBoarder(newCell("12.00", ffWhiteNormal7, Element.ALIGN_CENTER), 0f)
            }));
            table.addCell(addTableToCell(new float[]{1, 1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                    setBoarder(newCell("4", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                    addTableToCell(new float[]{1, 1}, Element.ALIGN_CENTER, new PdfPCell[]{
                            setBoarder(newCell("$", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                            setBoarder(newCell("4.00", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
                    }),
                    setBoarder(newCell("16.00", ffBlackNormal8, Element.ALIGN_CENTER), 0f)
            }));
        }

        table.addCell(addTableToCell(new float[]{1f, 1}, Element.ALIGN_LEFT, new PdfPCell[]{
            setBoarder(newCell("WEIGHT OF SHIPMENT", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                setBoarder(newCell("(Weight tickets attached)", ffBlackNormal8, Element.ALIGN_CENTER), 0f)
        }));
        table.addCell(addTableToCell(new float[]{2f, 1f}, Element.ALIGN_LEFT, new PdfPCell[]{
                setBoarder(newCell("TOTAL CONTAINER CHARGES", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 0f, 0f,
                        0f,
                        1f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
        }));
        table.addCell(addTableToCell(new float[]{2f, 1f}, Element.ALIGN_LEFT, new PdfPCell[]{
                setBoarder(newCell("TOTAL PACKING CHARGES", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 0f, 0f, 0f,
                        1f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
        }));
        table.addCell(addTableToCell(new float[]{2f, 1f}, Element.ALIGN_LEFT, new PdfPCell[]{
                setBoarder(newCell("TOTAL UNPACKING CHARGES", ffWhiteNormal7, Element.ALIGN_CENTER, "#000000"), 0f, 0f,
                        0f,
                        1f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)
        }));
        return table;
    }

    public PdfPTable addCrap(){
        PdfPTable table = new PdfPTable(new float[]{1, 0.02f, 1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.setWidthPercentage(100);

        PdfPCell cell;
        cell = new PdfPCell(addTableToCell(new float[]{0.8f, 1, 0.4f, 0.2f, 0.8f, 1, 0.1f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("Gross Weight ", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("Lbs.", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("Weighmaster", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)

        }));
        table.addCell(setBoarder(cell, 0f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 1, 0.4f}, Element.ALIGN_CENTER, new PdfPCell[]{
                setBoarder(newCell("PACKING", ffBlackNormal9, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                setBoarder(newCell("hours        @     ", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f)
        }));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{0.8f, 1, 0.4f, 0.2f, 0.8f, 1, 0.1f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("Tare Weight ", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("Lbs.", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("Weighmaster", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f)

        }));
        table.addCell(setBoarder(cell, 0f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 1, 0.4f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("UNPACKING", ffBlackNormal9, Element.ALIGN_LEFT), 0.5f, 0f, 0f, 0.5f),
                setBoarder(newCell("hours        @     ", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0f, 0.5f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0f, 0.5f)
        }));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1.1f, 1.7f, 0.3f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("Actual weight of shipment", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("Lbs.", ffBlackNormal8, Element.ALIGN_LEFT), 0f)
        }));
        table.addCell(setPadding(setBoarder(cell, 0f), 0f, 0f, 0f, 15f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 1, 0.4f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0.5f, 0f, 0f, 0f),
                setBoarder(newCell("TOTAL $", ffBlackNormal9, Element.ALIGN_RIGHT), 0f, 0.5f, 0f, 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0f, 0f),

                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("Sales Tax $", ffBlackNormal10, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f)
        }));
        table.addCell(setPadding(setBoarder(cell, 0f), 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(
                        newCell("INSURANCE/FULL VALUE PROTECTION", ffBlackNormal12, Element.ALIGN_LEFT), 0f),
                        setBoarder(addTableToCell(new float[]{1.02f, 0.18f, 0.01f}, Element.ALIGN_RIGHT, new PdfPCell[]{
                                setBoarder(newCell("The shipper declares the actual case value of this shipment to be $",
                                        ffBlackNormal8, Element.ALIGN_LEFT),0f),
                                setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                                setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f)

                }), 0f),
                setBoarder(addTableToCell(new float[]{0.2f, 1.1f, 0.9f, 1.8f, 0.7f, 0.05f}, Element.ALIGN_LEFT, new
                        PdfPCell[]{
                        setPadding(setBoarder(newCellWithImage(mContext, android.R.drawable.checkbox_off_background,
                                10f, 10f), 0f), 2f, 2f, 3.7f, 0f),
                        setBoarder(newCell("Transit Rate $", ffBlackNormal9, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100.00 Premium $", ffBlackNormal9, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f)
                }), 0f),

                setBoarder(addTableToCell(new float[]{0.2f, 1.1f, 0.7f, 1.7f, 0.4f, 0.6f}, Element.ALIGN_LEFT, new
                        PdfPCell[]{
                        setPadding(setBoarder(newCellWithImage(mContext, android.R.drawable.checkbox_off_background,
                                10f, 10f),
                                0f), 2f, 2f, 3.7f, 0f),
                        setBoarder(newCell("Storage Rate $", ffBlackNormal9, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100.00 Premium $", ffBlackNormal9, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal9, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per mo.", ffBlackNormal9, Element.ALIGN_LEFT), 0f),
                }), 0f),
                setPadding(setBoarder(addTableToCell(new float[]{0.3f, 0.09f, 0.85f, 0.015f}, Element.ALIGN_LEFT, new
                        PdfPCell[]{
                        setBoarder(newCell("Authorized Signature ", ffBlackNormal10, Element.ALIGN_CENTER), 0f),
                        setPadding(setBoarder(newCellWithImage(mContext, android.R.drawable.checkbox_off_background,
                                10f, 10f), 0f), 2f, 2f, 7f, 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT), 0f)
                }), 0f), 0f, 0f, 0f, 15f)

        }));
        table.addCell(setBoarder(cell, 1f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1, 0.2f}, Element.ALIGN_CENTER, new PdfPCell[]{

                addTableToCell(new float[]{0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("TIME RECORD", ffWhiteBold10, Element.ALIGN_CENTER, "#000000"),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f)
                }),

                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),

                setBoarder(addTableToCell(new float[]{1, 1, 0.5f, 0.1f, 1, 1, 0.5f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("\nStart", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                setBoarder(newCell("\nFinish", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                }), 0f),

                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),

                setBoarder(addTableToCell(new float[]{1, 1, 0.5f, 0.1f, 1, 1, 0.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("\nLeft Whse", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("\nBack Whse", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                }), 0f),

                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),

                setBoarder(addTableToCell(new float[]{1.3f, 0.7f, 0.5f, 0.1f, 1.4f, 0.6f, 0.5f}, Element.ALIGN_CENTER,
                        new
                        PdfPCell[]{
                        setBoarder(newCell("\nTravel Time", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("\nDriving Hours", ffBlackNormal8, Element.ALIGN_LEFT),0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_LEFT),0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("A.M.\nP.M.", ffBlackNormal8, Element.ALIGN_CENTER),0f),
                }), 0f),

                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),

                setBoarder(addTableToCell(new float[]{1.5f, 1.5f, 0.1f, 1, 0.05f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("TOTAL HOURS", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("@", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f)
                }), 0f),

                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f),
                setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0.5f, 0f, 0f, 0f)
        }));
        table.addCell(setBoarder(cell, 0.5f));
        return table;
    }

    public PdfPTable addCrap2(){
        PdfPTable table = new PdfPTable(new float[]{1f, 0.02f, 1f});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setBoarder(addTableToCell(new float[]{1f, 1f, 1f, 1f}, Element.ALIGN_CENTER, new
                PdfPCell[]{
                setBoarder(newCell("VALUATION OPTIONS", ffBlackNormal10, Element.ALIGN_CENTER), 0f),
                setBoarder(newCell("Initial your choice", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                setBoarder(newCell("Transportation no additional charge", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                setBoarder(newCell("Rate", ffBlackNormal8, Element.ALIGN_CENTER), 0f)
            }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Basic: 60cents/lb./art.", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Actual Cash Value", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Full Value", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("No Deductible", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Deductible of $250", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1.1f, 1f, 0.1f, 0.4f, 0.5f, 0.1f, 0.9f, 0.05f}, Element
                        .ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Deductible of $500", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("per $100", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("$", ffBlackNormal7, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f)
                }), 0f),
                setBoarder(addTableToCell(new float[]{1f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("BY SIGNING THIS FROM, YOU ARE WAIVING CERTAIN VALUABLE COVERAGE" +
                                " WHICH PROTECTS YOUR POSSESSIONS ABOVE THE MINIMUM AMOUNTS SET BY LAW." +
                                " PLEASE READ CAREFULLY.", ffBlackNormal7, Element.ALIGN_LEFT), 0f),

                }), 0f)
        });
        table.addCell(setPadding(setBoarder(cell, 0f), 0f, 0f, 0f, 0f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Transportation", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("miles", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("lbs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per cwt.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Vans", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("mrn", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("hrs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per hr.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("men extra", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("hrs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per hr.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Overtime rate", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("hrs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per hr.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Cartage (in or out)", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("miles", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("lbs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per cwt.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Warehouse handling", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("lbs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per cwt.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Storage in transit", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("days", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("lbs.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per cwt.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{1.8f, 0.7f, 0.7f, 0.7f, 0.8f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Storage", ffBlackNormal10, Element.ALIGN_LEFT), 0.5f, 0f, 0.5f, 0.5f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_RIGHT), 0f, 0.5f, 0.5f, 0.5f),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("per mo.", ffBlackNormal8, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                })
        });
        table.addCell(cell);
        return table;
    }

    public PdfPTable addCrap3(){
        PdfPTable table = new PdfPTable(new float[]{1f, 0.02f, 1f});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setPadding(setBoarder(addTableToCell(new float[]{1f, 2.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Shipper's Signature: ", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_CENTER), 0f, 0f, 0f, 0.5f),
                }), 0f), 0f, 0f, 0f, 3f),
                setBoarder(newCell("ALL CHARGES PAYABLE IN CASH, CERTIFIED HECK, OR CREDIT CARD BEFORE " +
                                "PROPERTY IS RELINQUISHED BY CARRIER.", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0.5f),
                setBoarder(newCell("I have read this contract, understand and agree to agree to the limit of " +
                        "liability as set forth above, to the provisions on both sides and received a copy.",
                        ffBlackNormal7, Element.ALIGN_LEFT), 0f)
        });
        table.addCell(setPadding(setBoarder(cell, 0f), 0f, 0f, 0f, 0f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                addTableToCell(new float[]{2f, 0.410f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        newCell("TOTAL", ffBlackNormal9, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{2f, 0.410f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        newCell("DEPOSIT", ffBlackNormal9, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                addTableToCell(new float[]{2f, 0.410f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        newCell("BALANCE DUE", ffBlackNormal9, Element.ALIGN_RIGHT),
                        newCell("", ffBlackNormal8, Element.ALIGN_RIGHT)
                }),
                setBoarder(addTableToCell(new float[]{1f}, Element.ALIGN_CENTER, new PdfPCell[]{
                        setBoarder(newCell("Goods received in good condition except as noted hereon.", ffBlackNormal7,
                                Element.ALIGN_RIGHT), 0f)
                }), 0f)
        });
        table.addCell(setBoarder(cell, 0f));
        return table;
    }

    public PdfPTable addCrap4(){
        PdfPTable table = new PdfPTable(new float[]{1f, 0.02f, 1f});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.setWidthPercentage(100);
        PdfPCell cell;
        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setPadding(setBoarder(addTableToCell(new float[]{0.45f, 0.2f, 2.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Sign Here", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCellWithImage(mContext, R.drawable.icn_signature, 36f, 36f), 0f, 0f, 0f, 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f)
                }), 0f), 0f),
                setBoarder(newCell("            Customer", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                setPadding(setBoarder(addTableToCell(new float[]{1.2f, 1.5f,0.5f, 1.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Receipt for Goods", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("Date", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f)
                }), 0f), 0f),
                setPadding(setBoarder(addTableToCell(new float[]{0.7f, 1f, 1f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("Driver", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("WHITE - CARRIER COPY", ffBlackNormal7, Element.ALIGN_RIGHT), 0f),
                }), 0f), 0f)
        });
        table.addCell(setPadding(setBoarder(cell, 0f), 0f));

        cell = setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_CENTER), 0f);
        table.addCell(cell);

        cell = addTableToCell(new float[]{1}, Element.ALIGN_CENTER, new PdfPCell[]{
                setPadding(setBoarder(addTableToCell(new float[]{0.45f, 0.2f, 2.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Delivery Receipt", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCellWithImage(mContext, R.drawable.icn_signature, 36f, 36f), 0f, 0f, 0f, 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f)
                }), 0f), 0f),
                setBoarder(newCell("            Customer", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                setPadding(setBoarder(addTableToCell(new float[]{1.2f, 1.5f,0.5f, 1.5f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("Received Payment For Company", ffBlackNormal8, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f),
                        setBoarder(newCell("Date", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal8, Element.ALIGN_JUSTIFIED), 0f, 0f, 0f, 0.5f)
                }), 0f), 0f),
                setPadding(setBoarder(addTableToCell(new float[]{1f, 0.6f, 1f}, Element.ALIGN_CENTER, new
                        PdfPCell[]{
                        setBoarder(newCell("YELLOW - CUSTOMER COPY", ffBlackNormal7, Element.ALIGN_LEFT), 0f),
                        setBoarder(newCell("Driver", ffBlackNormal8, Element.ALIGN_CENTER), 0f),
                        setBoarder(newCell("", ffBlackNormal7, Element.ALIGN_RIGHT), 0f),
                }), 0f), 0f)
        });
        table.addCell(setBoarder(cell, 0f));
        return table;
    }

    public PdfPTable addSign() {
        PdfPTable table = new PdfPTable(new float[]{1, 1, 1});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_BOTTOM);
        table.setWidthPercentage(100);

        PdfPCell cell;

        cell = new PdfPCell(addTableToCell(new float[]{1, 0.3f}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell("Customer Service", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f, 0f, 0f, 0.5f), setBoarder
                (newCell(null, null, 0), 0f)}));
        cell.setPaddingTop(30f);
        table.addCell(setBoarder(cell, 0f));

//        cell = new PdfPCell();
//        cell = setBoarder(cell, 0f, 0f, 0f, 0.5f);
//        cell.setImage(getImage(mContext, R.drawable.logo, 237f, 48f));
//        table.addCell(cell);

        cell = new PdfPCell(addTableToCell(new float[]{1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder(newCell
                (null, null, 0), 0f), setBoarder(newCellWithImage(mContext, sign, 237f, 48f), 0f, 0f, 0f, 0.5f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell(null, null, 0), 0f), setBoarder(newCell(stopDate, ffBlackNormal10, Element.ALIGN_BOTTOM),
                0f, 0f, 0f, 0.5f)}));
        cell.setPaddingTop(30f);
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{1, 0.3f}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell("Customer Name", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f), setBoarder(newCell(null, null,
                0), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        cell = setBoarder(newCell(null, null, 0), 0f);
        table.addCell(setBoarder(cell, 0f));

        cell = new PdfPCell(addTableToCell(new float[]{0.3f, 1}, Element.ALIGN_BOTTOM, new PdfPCell[]{setBoarder
                (newCell(null, null, 0), 0f), setBoarder(newCell("Date", ffBlackNormal10, Element.ALIGN_BOTTOM), 0f)}));
        table.addCell(setBoarder(cell, 0f));

        return table;
    }


    public PdfPCell setBoarder(PdfPCell _cell, float _left, float _right, float _top, float _bottom) {
        _cell.setBorderWidthLeft(_left);
        _cell.setBorderWidthBottom(_bottom);
        _cell.setBorderWidthRight(_right);
        _cell.setBorderWidthTop(_top);
        return _cell;
    }

    public PdfPCell setBoarder(PdfPCell _cell, float _boarder) {
        _cell.setBorderWidth(_boarder);
        return _cell;
    }

    public PdfPCell setBoarder(PdfPCell _cell, float _boarder, BaseColor _baseColor) {
        _cell.setBorderWidth(_boarder);
        _cell.setBorderColor(_baseColor);
        return _cell;
    }

    public PdfPCell setPadding(PdfPCell _cell, float _left, float _right, float _top, float _buttom) {
        _cell.setPaddingLeft(_left);
        _cell.setPaddingBottom(_buttom);
        _cell.setPaddingRight(_right);
        _cell.setPaddingTop(_top);
        return _cell;
    }

    public PdfPCell setPadding(PdfPCell _cell, float _padding) {
        _cell.setPadding(_padding);
        return _cell;
    }

    public PdfPCell addTableToCell(float[] _floats, int _align, PdfPCell[] _pdfPCell) {
        PdfPTable table = new PdfPTable(_floats);
        table.getDefaultCell().setHorizontalAlignment(_align);
        table.setWidthPercentage(100);
        for (PdfPCell a_pdfPCell : _pdfPCell) {
            table.addCell(a_pdfPCell);
        }
        PdfPCell cell;
        cell = new PdfPCell(table);
        cell.setPadding(0f);
        return cell;
    }

    public PdfPCell newCell(String _s, Font _font, int _align, String _color) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(_s, _font));
        cell.setHorizontalAlignment(_align);
        if (!TextUtils.isEmpty(_color)) {
            cell.setBackgroundColor(WebColors.getRGBColor(_color));
        }
        return cell;
    }

    public PdfPCell newCell(String _s, Font _font, int _align) {
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(_s, _font));
        cell.setHorizontalAlignment(_align);
        return cell;
    }

    public PdfPCell addToCell(PdfPCell _cell, String _s, Font _font) {
        Phrase firstLine = new Phrase(_s, _font);
        _cell.addElement(firstLine);
        return _cell;
    }

    public PdfPCell newCellWithImage(Context _context, int _logo, float _height, float _width) {
        PdfPCell cell;
        cell = new PdfPCell();
        cell.setImage(getImage(_context, _logo, _height, _width));
        return cell;
    }

    public PdfPCell newCellWithImage(Context _context, Bitmap _logo, float _height, float _width) {
        PdfPCell cell;
        cell = new PdfPCell();
        cell.setImage(getImage(_context, _logo, _height, _width));
        return cell;
    }


    public Paragraph addSpace(float _space) {
        Paragraph paragraph = new Paragraph(_space);
        paragraph.setSpacingBefore(_space);
        return paragraph;
    }

    public Paragraph addTextLine(String s, Font _font, int _alignLeft) {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(_alignLeft);
        paragraph.add(new Anchor(s, _font));
        return paragraph;
    }

    public Image getImage(Context _context, int _image, float _height, float _width) {
        Drawable d = ContextCompat.getDrawable(_context, _image);
        BitmapDrawable bitDw = ((BitmapDrawable) d);
        Bitmap bmp = bitDw.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = null;
        try {
            image = Image.getInstance(stream.toByteArray());
        } catch (BadElementException | IOException e) {
            e.printStackTrace();
        }
        if (image != null) {
            image.scaleAbsolute(_height, _width);
        }
        return image;
    }

    public Image getImage(Context _context, Bitmap _image, float _height, float _width) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Image image = null;
        if (_image != null) {
            _image.compress(Bitmap.CompressFormat.PNG, 100, stream);
            try {
                image = Image.getInstance(stream.toByteArray());
            } catch (BadElementException | IOException e) {
                e.printStackTrace();
            }
            if (image != null) {
                image.scaleAbsolute(_height, _width);
            }
        }
        return image;
    }
}

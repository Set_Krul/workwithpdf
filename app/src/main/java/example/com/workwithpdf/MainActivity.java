package example.com.workwithpdf;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.itextpdf.text.DocumentException;
import com.joanzapata.pdfview.PDFView;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    PDFView pdfView;
    Button btnNext;
    Button btnSign;
    View vPdfTitleContainer;
    FrameLayout mPdfContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPdfContainer = (FrameLayout) findViewById(R.id.pdf_container_PVF);
        vPdfTitleContainer = findViewById(R.id.pdfTitleContainer_FC);
        btnNext = (Button) findViewById(R.id.btnNext_CF);
        btnSign = (Button) findViewById(R.id.btnSign_CF);
        btnSign.setOnClickListener(this);
        generatePdf(null, 200);
    }

    private void generatePdf(Bitmap _signature, int delay) {
        Observable.timer(delay, TimeUnit.MILLISECONDS).map(aLong -> {
            File file = null;
            try {
                file = new CreateNew(this, _signature).createPdf();
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
                return null;
            }
            return file;
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showPdf, throwable -> {
        });
    }

    private void showPdf(File _file) {
        if (_file.exists()) {
            mPdfContainer.removeAllViews();
            pdfView = new PDFView(this, null);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            pdfView.setLayoutParams(params);

            pdfView.setZOrderOnTop(true);    // necessary
            SurfaceHolder sfhTrackHolder = pdfView.getHolder();
            sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);

            mPdfContainer.addView(pdfView);
            pdfView.fromFile(_file).showMinimap(true)
                    .enableSwipe(true).load();
            pdfView.setZoomCallBack(zoom -> {
                vPdfTitleContainer.setVisibility(zoom > 1 ? View.GONE : View.VISIBLE);
                btnNext.setVisibility(zoom > 1 ? View.GONE : View.VISIBLE);
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSign_CF:
                Bitmap bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.sign);
                generatePdf(bm, 100);
                break;
        }
    }
}
